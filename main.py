import logging as log
from argparse import ArgumentParser
from keras.models import load_model
from data_generator import DataGenerator
from models import *
from callbacks import *
from utils.utils import dummy_generator, configure_logger

def changeDirectory(dir, type):
	from time import gmtime, strftime
	import os
	import sys

	if dir == None:
		directoryName = strftime(type + "_%Y%m%d_%H%M%S", gmtime())
	else:
		directoryName = dir

	print("Changing to working directory:", directoryName)
	if os.path.exists(directoryName):
		print("Directory already exists, files may be overwritten. Confirm [y/n].")
		choice = input().lower().strip()
		if not choice == "y":
			sys.exit(0)
	else:
		os.makedirs(directoryName)
	os.chdir(directoryName)

def parseArguments():
	parser = ArgumentParser()
	parser.add_argument("type", type=str, default="train")
	parser.add_argument("train_json", type=str, help="Path to a JSON file that contains training labels and paths to"
		+ " the audio files.")
	parser.add_argument("test_json", type=str, help="Path to a JSON file that contains validation/test labels and"
		+ " paths to the audio files. This is used in training for validation data and in testing for testing data.")
	parser.add_argument("--dir", type=str, default=None, help="Working directory. If none is set a new one is created"
		+ " based on the current date and hour")
	parser.add_argument("--file", type=str, help="File used to import/export a model.", default="model_weights.h5")
	parser.add_argument("--new_file", type=str, help="File used to export a new model in a retrain", \
		default="model_weights_retrained.h5")
	parser.add_argument("--show_stderr", type=bool, default=True, help="Show log on stderr as well as file")
	args = parser.parse_args()

	assert args.type in ("train", "test", "retrain", "test_one_wav")
	return args

def getConvLayers(model):
	 # Find a way to iterate through all conv layers before GRU, just 1 layer is supported now.
	conv_layer = model.get_layer("conv_1d_1")
	if conv_layer is None:
		return []
	else:
		return [conv_layer]

# Uses Baidu's class to load the wavs and create spectrograms and so on... to look more into detail about this
def getDatagen(type, train_json, test_json, conv_layers):
	datagen = DataGenerator(max_duration=10)
	datagen.load_train_data(train_json)
	datagen.fit_train(100)

	if type in ("train", "retrain"):
		datagen.load_validation_data(test_json)
	else:
		datagen.load_test_data(test_json)

	# Save the convolutuon time steps output based on the characteristics of the conv layers before the GRU
	# This enables us to use any number of conv layers before it and we must apply all the functions in order.
	conv_params = []
	for layer in conv_layers:
		config = layer.get_config()
		filter_size = config["kernel_size"][0]
		border_mode = config["padding"]
		stride = config["strides"][0]
		conv_params.append([filter_size, border_mode, stride])
	datagen.conv_params = conv_params

	return datagen

def train(model, datagen):
	datagen.sortagradTrain = True
	numberOfEpochs = 2
	miniBatchSize = 16
	steps = datagen.getIterationsCount("train", miniBatchSize)
	#steps = 100

	# Register callbacks
	history = LossHistory("history.txt", 10)
	sortaGradCallback = SortaGradCallback(datagen)
	saveCallback = SaveModelCallback(model)

	#generator = dummy_generator(type="train", batch_size=5, time_samples=5, features=161)
	generator = datagen.iterate_single("train", miniBatchSize, steps)
	model.fit_generator(generator=generator, steps_per_epoch=steps, epochs=numberOfEpochs,
		callbacks=[history, sortaGradCallback, saveCallback],
		validation_data=datagen.iterate_single("validation", miniBatchSize),
		validation_steps=datagen.getIterationsCount("validation", miniBatchSize))

def test(model, datagen):
	# Get the relevant layers in the original model and construct one that only gets inputs and pops outputs.
	output_layer = model.get_layer("time_distributed_1")
	input_layer = model.get_layer("acoustic_input")
	# TODO: see why this outputs different values!!
	#testing_model = Model(inputs=input_layer.input, outputs=output_layer.output)
	testingFunction = K.function([input_layer.input, K.learning_phase()], [output_layer.output])
	testCallback = EvaluateCallback(decode_types=["lm_decoder", "ctc_decode_greedy", "ctc_decode_beam_search"], \
		evaluation_types=["character_error_rate", "word_error_rate"], file="evaluate.txt")

	#generator = dummy_generator(type="test, batch_size=5, time_samples=5, features=161)
	generator = datagen.iterate_single("test", miniBatchSize=5)
	for inputs, outputs in generator:
		input_data, input_length = inputs
		#result = testing_model.predict_on_batch(input_data)
		result = testingFunction([input_data, True])[0]
		testCallback(result, input_length, outputs)

	testCallback.on_end()

# TODO: Test just one wav without any labels at all.
def test_one_wav(model):
	raise

def main():
	args = parseArguments()
	# Warnings: Only one conv layer named "conv_1d_1" is expected
	# A custom loss function called "dummy_loss" is expected

	if args.type == "train":
		model = getModel0()
		# Find a way to iterate through all conv layers before GRU
		datagen = getDatagen(args.type, args.train_json, args.test_json, getConvLayers(model))

		changeDirectory(args.dir, args.type)
		configure_logger(show_stderr=args.show_stderr)

		log.info("Training...")
		train(model, datagen)
	elif args.type == "retrain":
		model = load_model(args.file, custom_objects={"dummy_loss" : dummy_loss})
		datagen = getDatagen(args.type, args.train_json, args.test_json, getConvLayers(model))

		changeDirectory(args.dir, args.type)
		configure_logger(show_stderr=args.show_stderr)

		log.info("Model loaded from: {}".format(args.file))
		log.info("Retraining...")
		train(model, datagen)
	else:
		model = load_model(args.file, custom_objects={"dummy_loss" : dummy_loss})
		datagen = getDatagen(args.type, args.train_json, args.test_json, getConvLayers(model))

		changeDirectory(args.dir, args.type)
		configure_logger(show_stderr=args.show_stderr)

		log.info("Model loaded from: {}".format(args.file))
		log.info("Testing...")
		test(model, datagen)

if __name__ == "__main__":
	main()
