import logging as log
import keras.backend as K
from keras.layers import *
from keras.models import *
from keras.optimizers import *

def ctc_lambda_func(args):
	y_pred, labels, input_length, label_length = args
	res = K.ctc_batch_cost(labels, y_pred, input_length, label_length)
	return K.mean(res)

def dummy_loss(y_true, y_pred):
	return y_pred

def compileModel(model):
	optimizer = SGD(nesterov=True, lr=2e-4, momentum=0.9, clipnorm=100)
	model.compile(loss={"dummy_loss": dummy_loss}, optimizer=optimizer)
	return model

def getModel0():
	log.info("Building model 0: 1 convolution")

	# Setup the network
	acoustic_input = Input(shape=(None, 161), name="acoustic_input", dtype="float32")
	output = Conv1D(10, kernel_size=3, name="conv1d_1", padding="same", strides=1, dtype="float32")(acoustic_input)
	output = TimeDistributed(Dense(29, name="dense", activation="softmax"),\
		name="time_distributed_1")(output)

	# Setup the CTC loss function layer
	y_true = Input(name="the_labels", shape=[None, ], dtype="int32")
	input_length = Input(name="input_length", shape=[1,], dtype="int32")
	label_length = Input(name="label_length", shape=[1,], dtype="int32")
	loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name="dummy_loss")([output, y_true, input_length, \
		label_length])

	model = Model(inputs=[acoustic_input, y_true, input_length, label_length], outputs=loss_out)
	return compileModel(model)

def getModel1(numGRU=3):
	log.info("Building model 1: 1 convolution + {} GRU".format(numGRU))
	initialization="glorot_uniform"

	acoustic_input = Input(shape=(None, 161), name="acoustic_input")
	conv_1d = Conv1D(1000, kernel_size=11,
		padding="valid", strides=2,
		activation="relu", kernel_initializer=initialization,
		name="conv_1d_1")(acoustic_input)
	output = BatchNormalization(name="bn_conv_1d")(conv_1d)

	for r in range(numGRU):
		gru_output = GRU(1000, activation="relu", name="rnn_{}".format(r + 1),
			kernel_initializer=initialization, return_sequences=True)(output)
		output = BatchNormalization(name="bn_rnn_{}".format(r + 1))(gru_output)

	network_output = TimeDistributed(Dense(29, name="dense", activation="softmax",
		kernel_initializer=initialization), name="time_distributed_1")(output)

	# Setup the CTC loss function layer
	labels = Input(name="the_labels", shape=[None, ], dtype="int32")
	output_lens = Input(name="output_lens", shape=[1], dtype="int32")
	label_lens = Input(name="label_lens", shape=[1], dtype="int32")

	ctc_loss = Lambda(ctc_lambda_func, output_shape=(1,), name="dummy_loss")([network_output, labels, output_lens, \
		label_lens])
	model = Model(inputs=[acoustic_input, labels, output_lens, label_lens], outputs=[ctc_loss])
	return compileModel(model)
