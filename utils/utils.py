import numpy as np
import logging

# Constant data generator
def dummy_generator(type, batch_size, time_samples, features):
    import time
    assert type in ("train", "test")
    x = np.zeros((batch_size, time_samples, features), dtype="float64") # Time x Features
    numeric_labels = self.getBlankIndex() * np.ones((batch_size, time_samples, ), dtype="int32") # 33
    input_length = np.ones((batch_size, 1, ), dtype="int32") * time_samples # value is shape after conv layer
    label_length = np.ones((batch_size, 1, ), dtype="int32")
    error = np.zeros((batch_size, 1, ), dtype="int32")

    if type == "train":
        inputs = [x, numeric_labels, input_length, label_length]
        outputs = error
    else:
        inputs = [x, input_length]
        outputs = numeric_labels
    ret = (inputs, outputs)

    while True:
        yield ret
        time.sleep(0.3)

def conv_output_length(input_length, filter_size, border_mode, stride,
                       dilation=1):
    """ Compute the length of the output sequence after 1D convolution along
        time. Note that this function is in line with the function used in
        Convolution1D class from Keras.
    Params:
        input_length (int): Length of the input sequence.
        filter_size (int): Width of the convolution kernel.
        border_mode (str): Only support `same` or `valid`.
        stride (int): Stride size used in 1D convolution.
        dilation (int)
    """
    if input_length is None:
        return None
    assert border_mode in {'same', 'valid'}
    dilated_filter_size = filter_size + (filter_size - 1) * (dilation - 1)
    if border_mode == 'same':
        output_length = input_length
    elif border_mode == 'valid':
        output_length = input_length - dilated_filter_size + 1
    return (output_length + stride - 1) // stride

def configure_logger(show_stderr):
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)
    # Add 2 handlers, file and stderr.
    rootLogger.addHandler(logging.FileHandler("logger.txt"))
    if show_stderr:
        rootLogger.addHandler(logging.StreamHandler())